#pragma once

#include "UI/ListViews/RedditContentListView.h"

namespace api
{
    class Subreddit
    {
    public:
        explicit Subreddit(Json::Value jsonData);

        const std::string& GetUrl() const { return m_Url; }
        const std::string& GetName() const { return m_Name; }
        const std::string& GetDisplayName() const { return m_DisplayName; }
        const std::string& GetTitle() const { return m_Title; }
        const std::string& GetPublicDesc() const { return m_PublicDesc; }
        const std::string& GetDescription() const { return m_Description; }
        int GetSubscribers() const { return m_Subscribers; }
        const std::string& GetIconPath() const { return m_IconPath; }
        const std::string& GetBannerPath() const { return m_BannerPath; }
        uint64_t GetCreatedTimeStamp() const { return m_CreatedTimestamp; }
        bool UserIsSubscriber() const { return m_UserIsSubscriber; }

        void SetUserIsSubscriber(bool subscriber);

    private:
        void UpdateDataFromJson(Json::Value jsonValue);
        std::string m_Url;
        std::string m_Name;
        std::string m_DisplayName;
        std::string m_Title;
        std::string m_PublicDesc;
        std::string m_Description;
        int m_Subscribers = 0;
        std::string m_IconPath;
        std::string m_BannerPath;
        uint64_t m_CreatedTimestamp = 0;
        bool m_UserIsSubscriber = false;
    };
}
