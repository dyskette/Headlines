#include "AppSettings.h"
#include "util/Helpers.h"

static AppSettings* s_Instance = nullptr;

AppSettings* AppSettings::Get()
{
    if (!s_Instance)
    {
        s_Instance = new AppSettings();
    }

    return s_Instance;
}

AppSettings::AppSettings()
{
    Load();
}

bool AppSettings::UseLibSecret()
{
#if WIN32
    return false;
#else
    return GetBool("use_libsecret", true);
#endif
}

bool AppSettings::GetBool(const std::string& name, bool defaultVal)
{
    Json::Value value = m_SaveData[name];
    return value.isBool() ? value.asBool() : defaultVal;
}

void AppSettings::SetBool(const std::string& name, bool value)
{
    m_SaveData[name] = value;
    Save();
}

int AppSettings::GetInt(const std::string& name, int defaultVal)
{
    Json::Value value = m_SaveData[name];
    return value.isInt() ? value.asInt() : defaultVal;
}

void AppSettings::SetInt(const std::string& name, int value)
{
    m_SaveData[name] = value;
    Save();
}

void AppSettings::Load()
{
    std::ifstream ifstream;
    ifstream.open(GetSettingsFilePath());
    if (ifstream.is_open())
    {
        std::string lineValue;
        std::stringstream stringValue;
        while (!ifstream.eof())
        {
            ifstream >> lineValue;
            stringValue << lineValue;
        }

        m_SaveData = util::Helpers::GetJsonValueFromString(stringValue.str());
    }
}

void AppSettings::Save()
{
    std::ofstream ofstream;
    ofstream.open(GetSettingsFilePath());

    Json::StreamWriterBuilder builder;
    Json::StreamWriter* writer = builder.newStreamWriter();
    writer->write(m_SaveData, &ofstream);
    ofstream.close();
}

std::string AppSettings::GetSettingsFilePath()
{
    std::stringstream ss;
    ss << g_get_user_config_dir() << "/headlines/app_settings.json";
    return ss.str();
}
