#include "UserMultiRedditsContentProvider.h"
#include "util/ThreadWorker.h"
#include <utility>
#include "API/api_fwd.h"
#include "API/RedditAPI.h"
#include "UI/Widgets/SearchResultWidget.h"

namespace ui
{
    UserMultiRedditsContentProvider::UserMultiRedditsContentProvider()
            : m_OnClick(nullptr)
    {

    }

    std::vector<RedditContentListItemRef> UserMultiRedditsContentProvider::GetContentInternal(std::string&, util::ThreadWorker* worker)
    {
        std::vector<api::MultiRedditRef> multiReddits = api::RedditAPI::Get()->GetUserMultiReddits();
        std::vector<RedditContentListItemRef> uiElements;

        for (const api::MultiRedditRef& multiReddit : multiReddits)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            SearchResultWidgetRef subredditUI = std::make_shared<SearchResultWidget>(multiReddit, m_OnClick);
            uiElements.push_back(subredditUI);
        }

        return uiElements;
    }

    std::string UserMultiRedditsContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No Multireddits found. Retry?";
    }

    void UserMultiRedditsContentProvider::SetItemClickHandler(std::function<void(const SearchResultWidget*)> onClick)
    {
        m_OnClick = std::move(onClick);
    }
}