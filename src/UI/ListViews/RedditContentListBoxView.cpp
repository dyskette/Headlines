#include "RedditContentListBoxView.h"
#include "UI/Widgets/RedditContentListItem.h"
#include "AppSettings.h"
#include "util/Signals.h"

namespace ui
{
    RedditContentListBoxView::RedditContentListBoxView(const RedditContentProviderRef& contentProvider)
        : RedditContentListView(contentProvider)
        , m_RoundedCornersSettingChangedHandler(util::s_InvalidSignalHandlerID)
    {

    }

    RedditContentListBoxView::~RedditContentListBoxView()
    {
        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }
    }

    void RedditContentListBoxView::CreateUI(Gtk::Box* parent, Gtk::Viewport* parentViewport)
    {
        RedditContentListView::CreateUI(parent, parentViewport);

        m_Listbox = new Gtk::ListBox();
        m_Listbox->get_style_context()->add_class("content");
        m_Listbox->set_margin_start(5);
        m_Listbox->set_margin_end(5);
        m_Listbox->signal_row_activated().connect([this](Gtk::ListBoxRow* row)
        {
            auto it = std::find_if(m_ContentItems.begin(), m_ContentItems.end(), [row](const RedditContentListItemRef& item) { return item->GetWidget() == row; });
            if (VERIFY(it != m_ContentItems.end(), "RedditContentListBoxView - Activated row that doesnt exist?"))
            {
                (*it)->OnActivate();
            }
        });

        m_ContentContainer->append(*m_Listbox);

        if (!AppSettings::Get()->GetBool("rounded_corners", false))
        {
            m_ContentContainer->get_style_context()->add_class("no_rounded_corners");
        }

        m_RoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([this](bool rounded)
        {
            if (rounded && m_ContentContainer->get_style_context()->has_class("no_rounded_corners"))
            {
                m_ContentContainer->get_style_context()->remove_class("no_rounded_corners");
            }
            else if (!rounded && !m_ContentContainer->get_style_context()->has_class("no_rounded_corners"))
            {
                m_ContentContainer->get_style_context()->add_class("no_rounded_corners");
            }
        });
    }

    void RedditContentListBoxView::AddContentToUI(std::vector<RedditContentListItemRef> newContent)
    {
        for (const RedditContentListItemRef& item : newContent)
        {
            m_ContentItems.push_back(item);
            item->CreateUI(m_Listbox);
        }
    }
}
