#pragma once

#include "API/Post.h"
#include "Page.h"

namespace util
{
    class ImageDownloader;
}

namespace ui
{
    class ImagePage : public Page
    {
    public:
        explicit ImagePage(const std::vector<util::ImageCollection>& imagePath);
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Reload() override { /*ignore*/ }
        virtual void Cleanup() override;
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override;
        virtual void OnSaveButton() override;

    private:
        //Glib::Dispatcher m_MainThreadDispatcher; //image downloaders must be created on the main thread
        const std::vector<util::ImageCollection>& m_Images;
        std::vector<Gtk::Picture*> m_Pictures;
        Gtk::Notebook* m_Notebook = nullptr;
        Gtk::Spinner* m_Spinner = nullptr;

        int m_OriginalPictureWidth;
        int m_OriginalPictureHeight;
        float m_Scale = 1.f;

        bool m_ImageDownloaded = false;
    };
}
