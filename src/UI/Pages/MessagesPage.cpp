#include "MessagesPage.h"
#include "Application.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "UI/ContentProviders/MessagesContentProvider.h"
#include "UI/ListViews/RedditContentListBoxView.h"
#include "util/SimpleThread.h"
#include "util/NotificationManager.h"
#include "UI/Widgets/HeaderBar.h"
namespace ui
{
    MessagesPage::MessagesPage()
        : Page(PageType::MessagesPage)
        , m_InboxContentProvider(std::make_shared<MessagesContentProvider>(MessagesType::Inbox))
        , m_InboxListView(std::make_shared<ui::RedditContentListView>(m_InboxContentProvider))
        , m_SentContentProvider(std::make_shared<MessagesContentProvider>(MessagesType::Sent))
        , m_SentListView(std::make_shared<ui::RedditContentListView>(m_SentContentProvider))
    {
    }

    Gtk::Box* MessagesPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/message_page.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("PostsView");

        m_Stack = (AdwViewStack*)builder->get_widget<Gtk::Widget>("SearchViewStack")->gobj();
        m_ViewSwitcherBar = (AdwViewSwitcherBar *)builder->get_widget<Gtk::Widget>("switcher_bar")->gobj();

        Gtk::Viewport* postsViewport;
        postsViewport = builder->get_widget<Gtk::Viewport>("InboxViewport");
        Gtk::Box* postsBox;
        postsBox = builder->get_widget<Gtk::Box>("InboxBox");
        m_InboxListView->CreateUI(postsBox, postsViewport);
        m_InboxListView->LoadContentAsync([]()
        {
            new util::SimpleThread([](util::ThreadWorker*)
            {
                util::NotificationManager::Get()->MarkMessagesRead();
            }, nullptr);
        });

        Gtk::Viewport* sentViewport;
        sentViewport = builder->get_widget<Gtk::Viewport>("SentViewport");
        Gtk::Box* sentBox;
        sentBox = builder->get_widget<Gtk::Box>("SentBox");
        m_SentListView->CreateUI(sentBox, sentViewport);
        m_SentListView->LoadContentAsync();

        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        return box;
    }

    void MessagesPage::OnHeaderBarCreated()
    {
        AdwViewSwitcherTitle* viewSwitcher = GetHeaderBar()->AddViewSwitcher(m_Stack);
        g_object_bind_property(viewSwitcher, "title-visible", m_ViewSwitcherBar, "reveal", GBindingFlags::G_BINDING_SYNC_CREATE);

        GetHeaderBar()->SetTitle("Messages");
    }

    void MessagesPage::Cleanup()
    {
    }

    void MessagesPage::Reload()
    {
        m_InboxListView->ClearContent();
        m_InboxListView->LoadContentAsync();
        m_SentListView->ClearContent();
        m_SentListView->LoadContentAsync();
    }

    UISettings MessagesPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            true,
            false,
            true,
            false,
            true
        };
    }
}
