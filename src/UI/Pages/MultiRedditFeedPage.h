#pragma once

#include "Page.h"
#include "API/api_fwd.h"
#include "UI/Widgets/MultiRedditBanner.h"

namespace ui
{
    class MultiRedditFeedPage : public Page
    {
    public:
        explicit MultiRedditFeedPage(const api::MultiRedditRef& multiReddit);
        ~MultiRedditFeedPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::MultiPosts; }
        virtual UISettings GetUISettings() const override;
    private:

        api::MultiRedditRef m_MultiReddit;
        ui::MultiRedditBanner m_MultiRedditBanner;
        ui::RedditContentListViewRef m_ListView;
        int m_RoundedCornersSettingChangedHandler = util::s_InvalidSignalHandlerID;
    };
}