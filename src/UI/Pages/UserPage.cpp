#include "UserPage.h"
#include "API/User.h"
#include "util/Helpers.h"
#include "UI/Widgets/HeaderBar.h"
#include "Application.h"
#include "AppSettings.h"
#include "util/SimpleThread.h"
#include "UI/ContentProviders/UserPostsContentProvider.h"
#include "UI/Pages/UserMultiRedditsPage.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "API/MultiReddit.h"

namespace ui
{
    UserPage::UserPage()
            : Page(PageType::UserPage)
            , m_PostsContentProvider(std::make_shared<UserPostsContentProvider>())
            , m_PostsView(std::make_shared<RedditContentListView>(m_PostsContentProvider))
            , m_CommentsView(std::make_shared<CommentsView>(CommentsViewType::UserComments))
    {
        struct ThreadStorage
        {
            api::UserRef user;
        };
        ThreadStorage* storage = new ThreadStorage();
        m_Threads.push_back(new util::SimpleThread([storage](util::ThreadWorker* worker)
        {
            if (!worker->IsCancelled())
            {
                storage->user = api::RedditAPI::Get()->GetCurrentUser();
            }
        },
        [this, storage](bool cancelled, util::SimpleThread* thread)
        {
            if (!cancelled)
            {
                m_Threads.erase(std::remove(m_Threads.begin(), m_Threads.end(), thread), m_Threads.end());
                SetUser(storage->user);
            }
            delete storage;
        }));

        SetupActions();
    }

    UserPage::UserPage(const api::UserRef& user)
        : Page(PageType::UserPage)
        , m_User(user)
        , m_PostsContentProvider(std::make_shared<UserPostsContentProvider>())
        , m_PostsView(std::make_shared<RedditContentListView>(m_PostsContentProvider))
        , m_CommentsView(std::make_shared<CommentsView>(CommentsViewType::UserComments))
    {
        SetupActions();
    }

    UserPage::UserPage(const std::string& username)
            : Page(PageType::UserPage)
            , m_PostsContentProvider(std::make_shared<UserPostsContentProvider>())
            , m_PostsView(std::make_shared<RedditContentListView>(m_PostsContentProvider))
            , m_CommentsView(std::make_shared<CommentsView>(CommentsViewType::UserComments))
    {
        struct ThreadStorage
        {
            api::UserRef user;
        };
        ThreadStorage* storage = new ThreadStorage();
        m_Threads.push_back(new util::SimpleThread([username, storage](util::ThreadWorker* worker)
        {
            if (!worker->IsCancelled())
            {
                storage->user = api::RedditAPI::Get()->GetUser(username);
            }
        },
        [this, storage](bool cancelled, util::SimpleThread* thread)
        {
            if (!cancelled)
            {
                m_Threads.erase(std::remove(m_Threads.begin(), m_Threads.end(), thread), m_Threads.end());
                SetUser(storage->user);
            }
            delete storage;
        }));

        SetupActions();
    }

    void UserPage::Cleanup()
    {
        for (util::SimpleThread* thread : m_Threads)
        {
            thread->Cancel();
        }

        g_signal_handler_disconnect(m_Stack, m_StackPageSwitchConnection);

        m_CommentsView.reset();
        m_PostsView.reset();
    }

    void UserPage::Reload()
    {
        if (strcmp(adw_view_stack_get_visible_child_name(m_Stack), "posts") == 0)
        {
            m_PostsView->ClearContent();
            m_PostsView->LoadContentAsync();
        }
        else
        {
            m_CommentsView->Reload();
        }
    }

    static void viewstack_visible_child_changed(AdwLeaflet*, GParamSpec*, gpointer user_data)
    {
        ((UserPage*)user_data)->VisibleChildChanged();
    }

    Gtk::Box* UserPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/user_page.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("UserViewBox");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        m_Spinner = builder->get_widget<Gtk::Spinner>("Spinner");
        if (m_User)
        {
            m_Spinner->set_visible(false);
        }

        m_Stack = (AdwViewStack*)builder->get_widget<Gtk::Widget>("Notebook")->gobj();
        m_SwitcherBar = (AdwViewSwitcherBar *)builder->get_widget<Gtk::Widget>("switcher_bar")->gobj();
        Gtk::Viewport* postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_PostsUserWidget = std::make_shared<ui::UserWidget>();
        m_PostsUserWidget->CreateUI(postsBox);
        m_PostsView->CreateUI(postsBox, postsViewport);

        Gtk::Viewport* commentsViewport = builder->get_widget<Gtk::Viewport>("CommentsViewport");
        Gtk::Box* commentsBox = builder->get_widget<Gtk::Box>("CommentsBox");
        m_CommentsUserWidget = std::make_shared<ui::UserWidget>();
        m_CommentsUserWidget->CreateUI(commentsBox);
        m_CommentsView->CreateUI(commentsViewport, commentsBox);

        m_StackPageSwitchConnection = g_signal_connect(m_Stack, "notify::visible-child", G_CALLBACK(viewstack_visible_child_changed), this);

        if (m_User)
        {
            SetUser(m_User);
        }

        return box;
    }

    void UserPage::SetUser(const ::api::UserRef& user)
    {
        if (m_Spinner)
        {
            ((Gtk::Box*)m_Spinner->get_parent())->remove(*m_Spinner);
            m_Spinner = nullptr;
        }

        m_User = user;
        m_PostsContentProvider->SetUser(user);
        m_CommentsView->SetUser(user);

        if (m_PostsUserWidget)
        {
            m_PostsUserWidget->SetUser(user);
            m_PostsUserWidget->SetVisible(true);
        }

        if (m_CommentsUserWidget)
        {
            m_CommentsUserWidget->SetUser(user);
            m_CommentsUserWidget->SetVisible(true);
        }

        m_PostsView->ClearContent();
        m_PostsView->LoadContentAsync();

        m_CommentsView->ClearComments();
        m_CommentsView->LoadCommentsAsync("");

        gtk_widget_set_visible((GtkWidget*)m_Stack, true);
    }

    SortType UserPage::GetSortType() const
    {
        if (strcmp(adw_view_stack_get_visible_child_name(m_Stack), "posts") == 0)
        {
            return SortType::UserPosts;
        }
        else
        {
            return SortType::UserComments;
        }
    }

    std::vector<SortType> UserPage::GetAllSortTypes() const
    {
        return { SortType::UserPosts, SortType::UserComments };
    }

    UISettings UserPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            true,
            false,
            true,
            false,
            true,
            false,
            false
        };
    }

    void UserPage::OnHeaderBarCreated()
    {
        AdwViewSwitcherTitle* viewSwitcher = GetHeaderBar()->AddViewSwitcher(m_Stack);
        g_object_bind_property(viewSwitcher, "title-visible", m_SwitcherBar, "reveal", GBindingFlags::G_BINDING_SYNC_CREATE);
    }

    void UserPage::VisibleChildChanged()
    {
        bool postsVisible = strcmp(adw_view_stack_get_visible_child_name(m_Stack), "posts") == 0;

        if (Application::Get())
        {
            GetHeaderBar()->UpdateSortMenuOptions();
            if (postsVisible)
            {
                GetHeaderBar()->SetCurrentPostsSortType(Application::Get()->GetUserPostsSorting());
            }
            else
            {
                GetHeaderBar()->SetCurrentCommentsSortType(Application::Get()->GetUserCommentsSorting());
            }
        }
    }

    void UserPage::SetupActions()
    {
        AddAction("Add to Multireddit", [this]()
        {
            ui::UserMultiRedditsPageRef userMultiRedditsPage = std::make_shared<ui::UserMultiRedditsPage>([this](const SearchResultWidget* widget)
            {
                const api::MultiRedditRef& multiReddit = widget->GetMultiReddit();
                if (multiReddit && std::find(multiReddit->GetSubreddits().begin(), multiReddit->GetSubreddits().end(), m_User->GetDisplayName()) == multiReddit->GetSubreddits().end())
                {
                    api::RedditAPI::Get()->AddUserToMultiReddit(m_User, multiReddit);
                    Application::Get()->NavigateBackwards();
                }
            });
            Application::Get()->AddPage(userMultiRedditsPage);
            userMultiRedditsPage->GetHeaderBar()->SetTitle("Select Multireddit");
        });
    }
}