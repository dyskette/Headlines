#include "VideoPage.h"
#include <libxml/parser.h>
#include <libxml/xmlstring.h>
#include <libxml/tree.h>
#include <util/Helpers.h>
#include <util/HttpClient.h>
#include "AppSettings.h"
#include "UI/Widgets/HeaderBar.h"

namespace ui
{
    VideoPage::VideoPage()
        : Page(PageType::VideoPage)
        , m_Url()
        , m_PendingTimeoutID(0)
        , m_MediaPrepared(false)
        , m_UrlPrepThread(nullptr)
        , m_MotionEventController(Gtk::EventControllerMotion::create())
    {
        m_VideoUrlReadyDispatcher.connect([this]()
        {
            OnVideoUrlReady();
        });
    }

    void VideoPage::SetupForDASH(const std::string& url)
    {
        m_UrlPrepThread = new std::thread([this, url]()
        {
            m_Url = url;

            //we need to calculate the duration of the video ourselves since gst cant work out the duration of dash playlists.
            xmlDocPtr dashDoc = GetDASHXml();
            std::string timeString = GetTimeStringFromDASHXml(dashDoc);
            double seconds = 0;
            ParseDASHTimeString(timeString, seconds);
            SetupDuration(seconds);

            m_VideoUrlReadyDispatcher.emit();
        });
    }

    void VideoPage::SetupForYoutube(const std::string& url)
    {
        m_UrlPrepThread = new std::thread([this, url]()
        {
            std::stringstream ss;
            ss << "youtube-dl --format \"bestvideo[height<=?480][fps<=?30]+bestaudio/best\" --get-url " << url;
            FILE* youtubeDl = popen(ss.str().c_str(), "r");

            ASSERT(youtubeDl, "Failed to open youtube-dl");

            char buffer[1024];
            char* output = fgets(buffer, sizeof(buffer), youtubeDl);
            pclose(youtubeDl);

            m_Url = output;
            m_VideoUrlReadyDispatcher.emit();
        });
    }

    static void gtk_video_notify_cb (GtkMediaStream*,
                                     GParamSpec* pspec,
                                     VideoPage* videoPage)
    {
        videoPage->OnNotify(pspec->name);
    }

    Gtk::Box*  VideoPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/video.ui");
        m_Overlay = builder->get_widget<Gtk::Overlay>("Overlay");
        m_Video = builder->get_widget<Gtk::Picture>("Picture");

        m_SeekBar = builder->get_widget<Gtk::Scale>("SeekBar");
        m_SeekBar->signal_value_changed().connect([this]()
        {
            if (!m_MediaFile || m_SeekBar->get_value() == (double)m_MediaFile->get_timestamp() / G_USEC_PER_SEC)
                return;

            m_MediaFile->seek((gint64)(m_SeekBar->get_value() * G_USEC_PER_SEC));

            //make sure to reset the controls timeout
            SetControlsVisible(true, m_MediaFile->get_playing());
        });

        m_CurrTimeLabel = builder->get_widget<Gtk::Label>("CurrTime");
        m_DurationLabel = builder->get_widget<Gtk::Label>("Duration");

        Gtk::Button* playButton = builder->get_widget<Gtk::Button>("PlayButton");
        m_PlayPauseImage = builder->get_widget<Gtk::Image>("PlayButtonImage");
        playButton->signal_clicked().connect([this]()
        {
            if (m_ControlsVisible)
            {
                m_MediaFile->get_playing() ? m_MediaFile->pause() : m_MediaFile->play();
            }

            SetControlsVisible(true, m_MediaFile->get_playing());
        });

        m_Spinner = builder->get_widget<Gtk::Spinner>("Spinner");
        m_Spinner->start();

        SetControlsVisible(true, false);

        m_MotionEventController->signal_motion().connect([this](double x, double y)
        {
            if (x != m_LastPointerX || y != m_LastPointerY)
            {
                m_LastPointerX = x;
                m_LastPointerY = y;
                SetControlsVisible(true, m_MediaFile && m_MediaFile->get_playing());
            }
        });
        m_MotionEventController->signal_leave().connect([this]()
        {
            if (!m_ControlsVisible)
            {
                SetControlsVisible(true, m_MediaFile && m_MediaFile->get_playing());
            }
        });

        m_Overlay->add_controller(m_MotionEventController);

        Gtk::Box* box = builder->get_widget<Gtk::Box>("Box");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        return box;
    }

    void VideoPage::OnVideoUrlReady()
    {
        Glib::RefPtr<Gio::File> file = Gio::File::create_for_uri(m_Url);
        m_MediaFile = Gtk::MediaFile::create();
        m_MediaFile->set_file(file);
        m_Video->set_paintable(m_MediaFile);
        Glib::RefPtr<Gdk::Surface> surface = m_Video->get_native()->get_surface();
        m_MediaFile->realize(surface);

        g_signal_connect (m_MediaFile->gobj(),
                          "notify",
                          G_CALLBACK(gtk_video_notify_cb),
                          this);

        m_MediaFile->play();
        if (m_MediaFile->get_error())
        {
            g_critical("%s", m_MediaFile->get_error().what());
        }

        m_UrlPrepThread->join();
        delete m_UrlPrepThread;
        m_UrlPrepThread = nullptr;
    }

    void VideoPage::Reload()
    {
        //ignore
    }

    void VideoPage::OnDeactivate()
    {
        if (m_MediaFile && m_MediaFile->get_playing())
        {
            m_MediaFile->pause();
        }
    }

    void VideoPage::Cleanup()
    {
        if (m_PendingTimeoutID != 0)
        {
            g_source_remove(m_PendingTimeoutID);
        }
        m_PendingTimeoutID = 0;

        m_MediaFile->pause();
        m_MediaFile->clear();
        m_MediaFile.reset();
    }

    SortType VideoPage::GetSortType() const
    {
        return SortType::None;
    }

    UISettings VideoPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            false,
            false,
            false,
            false,
            false
        };
    }

    std::string DurationToString(double seconds)
    {
        int hours = (int)(seconds / 60 / 60);
        seconds -= (double)hours * 60 * 60;
        int minutes = (int)(seconds / 60);
        seconds -= (double)minutes * 60;

        char buf[1024];
        if (hours > 0)
        {
            snprintf(buf, 1024, "<span foreground=\"#FFFFFF\">%d:%02d:%02.0f</span>", hours, minutes, seconds);
        }
        else
        {
            snprintf(buf, 1024, "<span foreground=\"#FFFFFF\">%02d:%02.0f</span>", minutes, seconds);
        }

        return std::string((const char*)&buf);
    }

    void VideoPage::OnNotify(const std::string& event)
    {
        if (event == "timestamp")
        {
            double timeStamp = (double)m_MediaFile->get_timestamp() / G_USEC_PER_SEC;
            m_SeekBar->set_value(timeStamp);
			m_CurrTimeLabel->set_markup(DurationToString(timeStamp));
        }
        else if (event == "playing")
        {
            if (!m_MediaFile->get_playing())
            {
                SetControlsVisible(true, false);
            }

            m_PlayPauseImage->set_from_icon_name(m_MediaFile->get_playing() ? "media-playback-pause-symbolic" : "media-playback-start-symbolic");
        }
        else if (event == "ended")
        {
            SetControlsVisible(true, false);
            m_PlayPauseImage->set_from_icon_name("media-playback-start-symbolic");
            m_MediaFile->seek(0);
        }
        else if (event == "duration")
        {
            int64_t seconds = m_MediaFile->get_duration() / G_USEC_PER_SEC;
            SetupDuration((double)seconds);
        }
        else if (event == "prepared")
        {
            m_MediaPrepared = true;
            SetControlsVisible(true, true);
            if (m_Spinner)
            {
                m_Overlay->remove_overlay(*m_Spinner);
                m_Spinner = nullptr;
            }
        }
        else if (event == "error")
        {
            g_critical("%s", m_MediaFile->get_error().what());
        }
    }

    static gboolean controlsTimeout(gpointer data)
    {
        VideoPage* videoPage = (VideoPage*)data;
        videoPage->SetControlsVisible(false, false);
        return false; // dont repeat
    }


    void VideoPage::SetControlsVisible(bool visible, bool addHideTimeout)
    {
        m_ControlsVisible = visible;
        m_PlayPauseImage->set_opacity(visible && m_MediaPrepared ? 1 : 0);
        m_SeekBar->set_visible(visible);
        m_DurationLabel->set_visible(visible);
        m_CurrTimeLabel->set_visible(visible);

        if (m_PendingTimeoutID != 0)
        {
            g_source_remove(m_PendingTimeoutID);
        }

        if (visible && addHideTimeout)
        {
            m_PendingTimeoutID = g_timeout_add_seconds(2, controlsTimeout, this);
        }
        else
        {
            m_PendingTimeoutID = 0; //assuming controls are only hidden by the timeout.
        }
    }

    xmlDocPtr VideoPage::GetDASHXml()
    {
        //streaming the file from the Gio::File doesnt work across all devices, so download it again *sigh*
        util::HttpClient httpClient;
        std::string dashPlaylist = httpClient.Get(m_Url, {});
        return xmlParseMemory(dashPlaylist.c_str(), (int)dashPlaylist.length());
    }

    void VideoPage::ParseDASHTimeString(std::string timeString, double& seconds)
    {
        int hours = 0;
        int minutes = 0;

        timeString.erase(0, 2); //remove PT
        auto it = timeString.find('H');
        if (it != std::string::npos)
        {
            hours = std::stoi(timeString.substr(0, it));
            timeString.erase(0, it + 1);
        }
        it = timeString.find('M');
        if (it != std::string::npos)
        {
            minutes = std::stoi(timeString.substr(0, it));
            timeString.erase(0, it + 1);
        }
        it = timeString.find('S');
        if (it != std::string::npos)
        {
            seconds = std::stod(timeString.substr(0, it));
            timeString.erase(0, it + 1);
        }

        seconds += minutes * 60;
        seconds += hours * 60 * 60;
    }

    std::string VideoPage::GetTimeStringFromDASHXml(xmlDocPtr dashDoc)
    {
        xmlNodePtr docRoot = xmlDocGetRootElement(dashDoc);
        xmlNodePtr node = util::Helpers::FindXmlNodeByName(docRoot, (const xmlChar*)"Period");
        for (xmlAttrPtr attribute = node->properties; attribute != nullptr; attribute = attribute->next)
        {
            if (strcmp((const char*)attribute->name, "duration") == 0)
            {
                return (const char*)attribute->children->content;
            }
        }

        return std::string();
    }

    void VideoPage::SetupDuration(double seconds)
    {
        Glib::RefPtr<Gtk::Adjustment> adjustment = Gtk::Adjustment::create(0, 0, seconds);
        m_SeekBar->set_adjustment(adjustment);
        m_DurationLabel->set_markup(DurationToString(seconds));
    }

    void VideoPage::Setup(const std::string& url)
    {
        std::string sanitizedUrl = util::Helpers::UnescapeHtml(url);
        auto it = sanitizedUrl.find('&');
        if (it != std::string::npos)
        {
            sanitizedUrl.resize(it);
        }

        if (sanitizedUrl.find("youtube") != std::string::npos || sanitizedUrl.find("youtu.be") != std::string::npos)
        {
            SetupForYoutube(sanitizedUrl);
        }
        else
        {
            SetupForDASH(sanitizedUrl);
        }

        GetHeaderBar()->SetTitle("Video");
    }
}