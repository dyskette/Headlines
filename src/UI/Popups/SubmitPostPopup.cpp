#include "SubmitPostPopup.h"
#include "AppSettings.h"
#include "Application.h"
#include "UserSubredditsPopup.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "API/RedditAPI.h"
#include "UI/Pages/CommentsPage.h"
#include "util/ThreadWorker.h"
#include "API/Flair.h"
#include "util/WebSocket.h"
#include "UI/ListViews/RedditContentListBoxView.h"
#include "UI/ContentProviders/UserSubredditsContentProvider.h"

namespace ui
{
    SubmitPostPopup::SubmitPostPopup(WritePostType type)
        : Popup(PageType::SubmitPost, false)
        , m_Type(type)
    {

    }

    void SubmitPostPopup::ShowSubredditPopover(Gtk::Widget* parent)
    {
        if (m_SubredditPopover && m_SubredditPopover->get_parent() != parent)
        {
            m_SubredditPopover->unparent();
            m_SubredditPopover->set_parent(*parent);
        }

        if (!m_SubredditPopover)
        {
            m_SubredditPopover = new Gtk::Popover();

            Gtk::ScrolledWindow* scrolledWindow = new Gtk::ScrolledWindow();
            scrolledWindow->set_size_request(280, 350);
            Gtk::Viewport* viewport = new Gtk::Viewport(Gtk::Adjustment::create(0,0,0), Gtk::Adjustment::create(0,0,0));
            viewport->set_vexpand(true);
            viewport->set_hexpand(true);
            scrolledWindow->set_child(*viewport);
            m_SubredditPopover->set_child(*scrolledWindow);
            Gtk::Box* subredditsBox = new Gtk::Box();
            viewport->set_child(*subredditsBox);
            UserSubredditsContentProviderRef contentProvider = std::make_shared<UserSubredditsContentProvider>();
            m_PopoverContents = std::make_shared<RedditContentListBoxView>(contentProvider);
            m_PopoverContents->CreateUI(subredditsBox, viewport);
            m_PopoverContents->GetListBox()->set_sort_func([](Gtk::ListBoxRow* a, Gtk::ListBoxRow* b)
            {
                return a->get_name() > b->get_name();
            });
            m_PopoverContents->LoadContentAsync();
            contentProvider->SetItemClickHandler([this](const SearchResultWidget* searchResult)
            {
                SetupSelectedSubreddit(searchResult->GetSubreddit());
                m_SelectSubredditButton->set_visible(false);
                m_SubredditPopover->hide();
            });

            m_SubredditPopover->set_parent(*parent);
        }

        m_SubredditPopover->show();
    }

    void SubmitPostPopup::OnPopupChoiceSelected(GtkResponseType response)
    {
        switch (response)
        {
            case GTK_RESPONSE_ACCEPT:
                TrySubmitPost();
                break;
            case GTK_RESPONSE_CANCEL:
                Close();
                break;
            default:
                g_warning("SubmitPostPopup::OnPopupChoiceSelected - Unhandled response type: %d", response);
                break;
        }
    }

    void SubmitPostPopup::ShowFileBrowser(FileBrowserType type, const std::function<void(const std::shared_ptr<Gio::File>&)>& onFileSelected)
    {
        Gtk::FileChooserDialog* fileChooser = new Gtk::FileChooserDialog("Choose Video...", Gtk::FileChooserDialog::Action::OPEN, true);

        Glib::RefPtr<Gtk::FileFilter> fileFilter = Gtk::FileFilter::create();
        switch (type)
        {
            case FileBrowserType::Image:
                fileFilter->add_mime_type("image/jpeg");
                fileFilter->add_mime_type("image/gif");
                fileFilter->add_mime_type("image/png");
                fileFilter->set_name("Image Files (Jpg/Jpeg/Gif/Png)");
                break;
            case FileBrowserType::Video:
                fileFilter->add_mime_type("video/quicktime");
                fileFilter->add_mime_type("video/mp4");
                fileFilter->set_name("Video Files (MP4/Quicktime)");
                break;
            default:
                ASSERT(false, "WritePostPage::ShowFileBrowser - Unhandled FileBrowserType");
                break;
        }

        fileChooser->add_button("Open", Gtk::ResponseType::ACCEPT);
        fileChooser->add_button("Cancel", Gtk::ResponseType::CANCEL);
        fileChooser->set_filter(fileFilter);
        fileChooser->signal_response().connect([fileChooser, onFileSelected](int response)
                                               {
                                                   if (response == Gtk::ResponseType::ACCEPT)
                                                   {
                                                       onFileSelected(fileChooser->get_file());
                                                   }

                                                   fileChooser->hide();
                                                   delete fileChooser;
                                               });

//        fileChooser->set_transient_for(*m_Dialog);
        fileChooser->show();
    }

    void SubmitPostPopup::TrySubmitPost()
    {
        switch (m_Type)
        {
            case WritePostType::Text:
            {
                std::string title = m_TitleEntry->get_text();
                std::string text = m_TextView->get_buffer()->get_text();

                if (!title.empty() && !text.empty())
                {
                    std::string error;
                    api::PostRef post = api::RedditAPI::Get()->SubmitTextPost(m_SelectedSubreddit->GetSubreddit(), title, text, m_NSFWButton->get_active(), m_SpoilerButton->get_active(), GetSelectedFlair(), error);
                    if (!post)
                    {
                        ShowErrorMessage(error);
                    }
                    else
                    {
                        Close();

                        ui::CommentsPageRef commentView = std::make_shared<ui::CommentsPage>();
                        Application::Get()->AddPage(commentView);
                        commentView->SetPost(post, "");
                    }
                }

                break;
            }
            case WritePostType::Link:
            {
                std::string title = m_TitleEntry->get_text();
                std::string text = m_TextView->get_buffer()->get_text();

                if (!title.empty() && !text.empty())
                {
                    std::string error;
                    api::PostRef post = api::RedditAPI::Get()->SubmitUrlPost(m_SelectedSubreddit->GetSubreddit(), title, text, m_NSFWButton->get_active(), m_SpoilerButton->get_active(), GetSelectedFlair(), error);
                    if (!post)
                    {
                        ShowErrorMessage(error);
                    }
                    else
                    {
                        Close();

                        ui::CommentsPageRef commentView = std::make_shared<ui::CommentsPage>();
                        Application::Get()->AddPage(commentView);
                        commentView->SetPost(post, "");
                    }
                }

                break;
            }
            case WritePostType::Image:
            {
                m_Mutex.lock();
                if (!m_Threads.empty())
                {
                    Application::Get()->ShowNotification("Wait for media to finish uploading");
                    m_Mutex.unlock();
                    return;
                }
                m_Mutex.unlock();

                std::string title = m_TitleEntry->get_text();
                if (!title.empty() && m_MediaResults.size() == 1)
                {
                    for (Gtk::Widget* widget = GetContentBox()->get_first_child(); widget != nullptr; widget = widget->get_next_sibling())
                    {
                        widget->set_visible(false);
                    }

                    Gtk::Spinner* spinner = new Gtk::Spinner();
                    spinner->set_size_request(100, 100);
                    spinner->set_halign(Gtk::Align::CENTER);
                    spinner->set_valign(Gtk::Align::CENTER);
                    spinner->set_hexpand(true);
                    spinner->set_vexpand(true);
                    GetContentBox()->append(*spinner);
                    spinner->set_visible(true);
                    spinner->start();

                    std::string error;
                    api::RedditAPI::Get()->SubmitImagePost(m_SelectedSubreddit->GetSubreddit(), title, m_MediaResults[0].first, m_MediaResults[0].second, m_NSFWButton->get_active(), m_SpoilerButton->get_active(), GetSelectedFlair(), [this](const api::PostRef& post)
                    {
                        Close();

                        if (!post)
                        {
                            Application::Get()->ShowNotification("Error submitting post.");
                        }
                        else
                        {
                            ui::CommentsPageRef commentView = std::make_shared<ui::CommentsPage>();
                            Application::Get()->AddPage(commentView);
                            commentView->SetPost(post, "");
                        }
                    }, error);

                    if (!error.empty())
                    {
                        ShowErrorMessage(error);
                    }
                }

                break;
            }
            case WritePostType::Video:
            {
                m_Mutex.lock();
                if (!m_Threads.empty())
                {
                    Application::Get()->ShowNotification("Wait for media to finish uploading");
                    m_Mutex.unlock();
                    return;
                }
                m_Mutex.unlock();

                ASSERT(m_MediaResults.size() == 1, "App currently only supports single image posts.");

                std::string title = m_TitleEntry->get_text();

                for (Gtk::Widget* widget = GetContentBox()->get_first_child(); widget != nullptr; widget = widget->get_next_sibling())
                {
                    widget->set_visible(false);
                }

                Gtk::Spinner* spinner = new Gtk::Spinner();
                spinner->set_size_request(100, 100);
                spinner->set_halign(Gtk::Align::CENTER);
                spinner->set_valign(Gtk::Align::CENTER);
                spinner->set_hexpand(true);
                spinner->set_vexpand(true);
                GetContentBox()->append(*spinner);
                spinner->set_visible(true);
                spinner->start();

                std::string error;
                api::RedditAPI::Get()->SubmitVideoPost(m_SelectedSubreddit->GetSubreddit(),
                                                       title,
                                                       m_VideoMediaResult.first,
                                                       m_VideoThumbnailMediaResult.first,
                                                       m_VideoMediaResult.second,
                                                       m_NSFWButton->get_active(),
                                                       m_SpoilerButton->get_active(),
                                                       GetSelectedFlair(),
                [this](const api::PostRef& post)
                {
                   Close();

                   if (!post)
                   {
                       Application::Get()->ShowNotification("Error submitting post.");
                   }
                   else
                   {
                       ui::CommentsPageRef commentView = std::make_shared<ui::CommentsPage>();
                       Application::Get()->AddPage(commentView);
                       commentView->SetPost(post, "");
                   }
                }, error);

                if (!error.empty())
                {
                    ShowErrorMessage(error);
                }

                break;
            }
        }
    }

    void SubmitPostPopup::UploadMedia(const std::shared_ptr<Gio::File>& file, bool video, bool thumbnail)
    {
        if (video)
        {
            // first we need to make a thumbnail and upload that.
            std::stringstream ss;
            ss << "ffmpeg -i \'" << file->get_path() << "\' -y -r 1 -frames 1 -t 00:00:01 -f image2 temp.png";
            int ret = system(ss.str().c_str());
            if (ret != 0)
            {
                g_warning("Error: %i while running ffmpeg", ret);
            }

            UploadMedia(Gio::File::create_for_path("./temp.png"), false, true);
        }

        if (!video)
        {
            m_ImagesBox->set_visible(true);
            Gtk::Overlay* overlay = new Gtk::Overlay();
            Gtk::Picture* picture = new Gtk::Picture();
            overlay->set_child(*picture);
            picture->set_file(file);

            Gtk::Spinner* spinner = new Gtk::Spinner();
            spinner->set_hexpand(true);
            spinner->set_vexpand(true);
            spinner->start();
            spinner->set_visible(true);
            overlay->add_overlay(*spinner);
            m_PreviewSpinners[file] = spinner;

            m_ImagesBox->append(*overlay);
        }

        UploadThread* thread = new UploadThread();

        thread->m_Worker = new util::ThreadWorker();
        thread->m_Thread = new std::thread([this, thread, file, video, thumbnail]()
                                           {
                                               thread->m_Worker->Run([this, file, video, thumbnail](util::ThreadWorker*)
                                                                     {
                                                                         std::pair<std::string, util::WebSocket*> mediaResult = api::RedditAPI::Get()->UploadMedia(file->get_path());
                                                                         std::lock_guard<std::mutex> lock(m_Mutex);

                                                                         if (video)
                                                                         {
                                                                             m_VideoMediaResult = mediaResult;
                                                                         }
                                                                         else if (thumbnail)
                                                                         {
                                                                             m_VideoThumbnailMediaResult = mediaResult;
                                                                         }
                                                                         else
                                                                         {
                                                                             m_MediaResults.push_back(mediaResult);
                                                                         }
                                                                     },
                                                                     [this, thread, file](bool cancelled, util::ThreadWorker*)
                                                                     {
                                                                         if (!cancelled)
                                                                         {
                                                                             std::lock_guard<std::mutex> lock(m_Mutex);
                                                                             m_CompletedThreads.push_back(thread);
                                                                             m_Threads.erase(std::remove(m_Threads.begin(), m_Threads.end(), thread), m_Threads.end());

                                                                             if (m_PreviewSpinners.find(file) != m_PreviewSpinners.end())
                                                                             {
                                                                                 m_PreviewSpinners[file]->stop();
                                                                                 m_PreviewSpinners.erase(file);
                                                                             }
                                                                             m_MainThreadDispatcher.emit();
                                                                         }
                                                                     });
                                           });

        std::lock_guard<std::mutex> lock(m_Mutex);
        m_Threads.push_back(thread);
    }

    void SubmitPostPopup::CheckValid()
    {
        switch (m_Type)
        {
            case WritePostType::Text:
            {
                SetHeaderButtonEnabled(GTK_RESPONSE_ACCEPT, m_TextView->get_buffer()->size() > 0 && m_TitleEntry->get_text_length() > 0);
                break;
            }
            case WritePostType::Link:
            {
                bool urlValid = m_TextView->get_buffer()->size() > 0 && m_TextView->get_buffer()->get_text().find("http") == 0;
                SetHeaderButtonEnabled(GTK_RESPONSE_ACCEPT, urlValid && m_TitleEntry->get_text_length() > 0);
                break;
            }
            case WritePostType::Image:
            {
                SetHeaderButtonEnabled(GTK_RESPONSE_ACCEPT, !m_MediaResults.empty() && m_TitleEntry->get_text_length() > 0);
                break;
            }
            case WritePostType::Video:
            {
                SetHeaderButtonEnabled(GTK_RESPONSE_ACCEPT, m_VideoMediaResult.second && m_VideoThumbnailMediaResult.second && m_TitleEntry->get_text_length() > 0);
                break;
            }
        }

    }

    void SubmitPostPopup::SetupSelectedSubreddit(const api::SubredditRef& subreddit)
    {
        if (!m_SelectedSubreddit)
        {
            m_SelectedSubreddit = std::make_shared<SearchResultWidget>(subreddit, [this](const SearchResultWidget*)
            {
                ShowSubredditPopover(m_SelectedSubreddit->GetWidget());
            });
            m_SelectedSubreddit->CreateUI(m_SubredditBox);
            m_SelectedSubreddit->GetWidget()->set_hexpand(false);
            m_SelectedSubreddit->GetWidget()->set_halign(Gtk::Align::START);
        }
        else
        {
            m_SelectedSubreddit->SetSubreddit(subreddit);
        }

        m_SubredditFlairs = api::RedditAPI::Get()->GetSubredditPostFlairs(subreddit);
        std::vector<Glib::ustring> flairTextList = {"None"};
        for (const api::FlairRef& flair : m_SubredditFlairs)
        {
            flairTextList.push_back(flair->GetText());
        }
        m_FlairDropDown->set_model(Gtk::StringList::create(flairTextList));
        m_FlairDropDown->set_sensitive(m_SubredditFlairs.size() > 0);
    }

    void SubmitPostPopup::ShowErrorMessage(const std::string& errorMsg)
    {
        Gtk::MessageDialog* confirmDialog = new Gtk::MessageDialog(errorMsg, false, Gtk::MessageType::ERROR, Gtk::ButtonsType::OK);
        confirmDialog->signal_response().connect([confirmDialog](int)
                                                 {
                                                     delete confirmDialog;
                                                 });

        confirmDialog->set_transient_for(GetDialog() ? *GetDialog() : *Application::Get()->GetGtkApplication()->get_active_window());
        confirmDialog->show();
    }

    api::FlairRef SubmitPostPopup::GetSelectedFlair()
    {
        guint flairIndex = m_FlairDropDown->get_selected();
        if (!m_SubredditFlairs.empty() && flairIndex > 0)
        {
            return m_SubredditFlairs[flairIndex - 1];
        }

        return api::FlairRef();
    }

    Gtk::Box* SubmitPostPopup::CreateUIInternal(AdwLeaflet* parent)
    {
        AddPopupAction(PopupAction{ "Cancel", GTK_RESPONSE_CANCEL, true, [this]()
        { Close(); }});
        AddPopupAction(PopupAction{ "Post", GTK_RESPONSE_ACCEPT, false, [this]()
        { TrySubmitPost(); }});

        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/write_post.ui");
        Gtk::Box* box = builder->get_widget<Gtk::Box>("Box");

        m_SubredditBox = builder->get_widget<Gtk::Box>("SubredditBox");

        m_SelectSubredditButton = builder->get_widget<Gtk::Button>("AddSubredditButton");
        m_SelectSubredditButton->signal_clicked().connect([this]()
                                                          {
                                                              ShowSubredditPopover(m_SelectSubredditButton);
                                                          });

        m_NSFWButton = builder->get_widget<Gtk::ToggleButton>("NSFW");
        m_SpoilerButton = builder->get_widget<Gtk::ToggleButton>("Spoiler");

        m_FlairDropDown = builder->get_widget<Gtk::DropDown>("FlairDropDown");
        m_FlairDropDown->set_model(Gtk::StringList::create({"Flair"}));
        m_FlairDropDown->set_sensitive(false);

        m_AddImageButton = builder->get_widget<Gtk::Button>("AddImageButton");
        m_AddImageButton->set_visible(m_Type == WritePostType::Image);
        m_AddImageButton->signal_clicked().connect([this]()
                                                   {
                                                       ShowFileBrowser(FileBrowserType::Image, [this](const std::shared_ptr<Gio::File>& file)
                                                       {
                                                           UploadMedia(file, false, false);
                                                           m_AddImageButton->set_visible(false);
                                                       });
                                                   });

        m_AddVideoButton = builder->get_widget<Gtk::Button>("AddVideoButton");
        m_AddVideoButton->set_visible(m_Type == WritePostType::Video);
        m_AddVideoButton->signal_clicked().connect([this]()
                                                   {
                                                       ShowFileBrowser(FileBrowserType::Video, [this](const std::shared_ptr<Gio::File>& file)
                                                       {
                                                           UploadMedia(file, true, false);
                                                           m_AddVideoButton->set_visible(false);
                                                       });
                                                   });

        m_TitleEntry = builder->get_widget<Gtk::Entry>("TitleEntry");
        m_TitleEntry->signal_changed().connect([this]()
                                               {
                                                   CheckValid();
                                               });

        m_TextView = builder->get_widget<Gtk::TextView>("TextView");
        m_TextView->set_visible(m_Type == WritePostType::Link || m_Type == WritePostType::Text);
        m_TextView->get_buffer()->signal_changed().connect([this]()
                                                           {
                                                               CheckValid();
                                                           });

        m_ContentLabel = builder->get_widget<Gtk::Label>("ContentLabel");
        switch (m_Type)
        {
            case WritePostType::Text:
                m_ContentLabel->set_text("Text:");
                break;
            case WritePostType::Link:
                m_ContentLabel->set_text("URL:");
                break;
            case WritePostType::Image:
                m_ContentLabel->set_text("Image(s):");
                break;
            case WritePostType::Video:
                m_ContentLabel->set_text("Video(s):");
                break;
        }

        m_ImagesBox = builder->get_widget<Gtk::Box>("ImagesBox");
        m_ImagesBox->set_visible(false);

        CreatePopup(parent, box, "Submit Post");

        return box;
    }
}
