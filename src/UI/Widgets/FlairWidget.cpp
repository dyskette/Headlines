#include "FlairWidget.h"
#include "API/Flair.h"

namespace ui
{
    FlairWidget::FlairWidget(const api::FlairRef& flair)
        : m_Flair(flair)
        , m_Box(nullptr)
    {

    }

    void FlairWidget::CreateUI(Gtk::Widget* parent)
    {
        m_Box = new Gtk::Box();
        m_Box->set_halign(Gtk::Align::START);
        Gtk::Label* label = new Gtk::Label();
        label->set_text(" " + m_Flair->GetText() + " ");
        label->set_wrap_mode(Pango::WrapMode::CHAR);
        label->set_wrap(true);
        label->set_justify(Gtk::Justification::LEFT);
        label->set_halign(Gtk::Align::START);
        Pango::AttrList attrList;
        Pango::AttrColor attrForegroundColour = m_Flair->GetTextColour().AsPangoForegroundColour();
        Pango::AttrColor attrBackgroundColour = m_Flair->GetColour().AsPangoBackgroundColour();
        attrList.insert(attrForegroundColour);
        attrList.insert(attrBackgroundColour);
        label->set_attributes(attrList);
        m_Box->append(*label);

        m_Parent = ((Gtk::Box*)parent);
        m_Parent->append(*m_Box);
    }

    void FlairWidget::RemoveUI()
    {
        m_Parent->remove(*m_Box);
    }
}