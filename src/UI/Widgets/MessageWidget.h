#pragma once

#include <util/Signal.h>
#include "RedditContentListItem.h"
#include "UI/ui_types_fwd.h"
#include "API/api_fwd.h"
#include "util/Colour.h"

namespace util
{
    class SimpleThread;
}

namespace ui
{
    class PictureWidget;
    class MessageWidget : public RedditContentListItem
    {
    public:
        explicit MessageWidget(const api::MessageRef& post);
        ~MessageWidget();
        virtual void CreateUI(Gtk::Widget* parent) override;
        virtual void RemoveUI() override;
        Gtk::Widget* GetWidget() override { return m_Card; }

        virtual int GetContentTop() override;
        virtual int GetContentBottom() override;
        virtual void SetActive(bool active) override;
    private:
        // UI Setup
        void SetupCard(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupIcon(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupTitleLabel(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupSourceLabel(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupBadges(const Glib::RefPtr<Gtk::Builder>& builder);
        void SetupContents(const Glib::RefPtr<Gtk::Builder>& builder);
        void AddCustomFlair(const std::string& text, const util::Colour& colour, const util::Colour& textColour);
        void SetupDateText(const Glib::RefPtr<Gtk::Builder>& builder);

        api::MessageRef m_Message;

        Gtk::Box* m_Card;
        PictureWidget* m_Icon = nullptr;
        Gtk::Box* m_BadgeBox = nullptr;
        Gtk::Box* m_SourceLabel = nullptr;
        std::vector<FlairWidgetRef> m_Badges;
        Gtk::Box* m_ContentsBox = nullptr;

        util::SimpleThread* m_IconThread = nullptr;

        int m_RoundedCornersSettingChangedHandler = util::s_InvalidSignalHandlerID;
    };
}
