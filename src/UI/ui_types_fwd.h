#pragma once

namespace ui
{
    enum class PageType
    {
        MainFeedPage,
        CommentsPage,
        SearchPage,
        SubredditPage,
        UserPage,
        SavedPage,
        UpvotedPage,
        DownvotedPage,
        GildedPage,
        HiddenPage,
        WriteCommentPage,
        VideoPage,
        UserSubredditsPage,
        ImagePage,
        SettingsPage,
        SubmitPost,
        MultiReddits,
        MultiRedditFeed,
        EditMultiReddit,
        CreateMultiReddit,
        MessagesPage,
        SendMessage
    };

    enum class SortType
    {
        None,
        Posts,
        Comments,
        SubredditPosts,
        UserPosts,
        UserComments,
        MultiPosts,
    };

    class Page;
    typedef std::shared_ptr<Page> PageRef;

    class RedditContentListView;
    typedef std::shared_ptr<RedditContentListView> RedditContentListViewRef;

    class RedditContentListItem;
    typedef std::shared_ptr<RedditContentListItem> RedditContentListItemRef;

    class PostWidget;
    typedef std::shared_ptr<PostWidget> PostWidgetRef;

    class SearchResultWidget;
    typedef std::shared_ptr<SearchResultWidget> SearchResultWidgetRef;

    class CommentUI;
    typedef std::shared_ptr<CommentUI> CommentWidgetRef;

    class MainFeedContentProvider;
    typedef std::shared_ptr<MainFeedContentProvider> MainFeedContentProviderRef;

    class CommentsView;
    typedef std::shared_ptr<CommentsView> CommentsViewRef;

    class CommentsPage;
    typedef std::shared_ptr<CommentsPage> CommentsPageRef;

    class HeaderBar;
    typedef std::shared_ptr<HeaderBar> HeaderBarRef;

    class SearchPage;
    typedef std::shared_ptr<SearchPage> SearchPageRef;

    class SubredditPage;
    typedef std::shared_ptr<SubredditPage> SubredditPageRef;

    class UserPage;
    typedef std::shared_ptr<UserPage> UserPageRef;

    class Sidebar;
    typedef std::shared_ptr<Sidebar> SidebarRef;

    class SavedPage;
    typedef std::shared_ptr<SavedPage> SavedPageRef;

    class UpvotedPostsPage;
    typedef std::shared_ptr<UpvotedPostsPage> UpvotedPostsPageRef;

    class DownvotedPostsPage;
    typedef std::shared_ptr<DownvotedPostsPage> DownvotedPostsPageRef;

    class GildedPostsPage;
    typedef std::shared_ptr<GildedPostsPage> GildedPostsPageRef;

    class HiddenPostsPage;
    typedef std::shared_ptr<HiddenPostsPage> HiddenPostsPageRef;

    class WriteCommentPopup;
    typedef std::shared_ptr<WriteCommentPopup> WriteCommentPageRef;

    class VideoPage;
    typedef std::shared_ptr<VideoPage> VideoPageRef;

    class UserPostsContentProvider;
    typedef std::shared_ptr<UserPostsContentProvider> UserPostsContentProviderRef;

    class Popup;
    typedef std::shared_ptr<Popup> PopupRef;

    class SubmitPostPopup;
    typedef std::shared_ptr<SubmitPostPopup> SubmitPostPopupRef;

    class UserSubredditsContentProvider;
    typedef std::shared_ptr<UserSubredditsContentProvider> UserSubredditsContentProviderRef;

    class UserSubredditsPopup;
    typedef std::shared_ptr<UserSubredditsPopup> UserSubredditsPopupRef;

    class ImagePage;
    typedef std::shared_ptr<ImagePage> ImagePageRef;

    class FlairWidget;
    typedef std::shared_ptr<FlairWidget> FlairWidgetRef;

    class MainFeedPage;
    typedef std::shared_ptr<MainFeedPage> MainFeedPageRef;

    class SettingsPage;
    typedef std::shared_ptr<SettingsPage> SettingsPageRef;

    class UserWidget;
    typedef std::shared_ptr<UserWidget> UserWidgetRef;

    class SubredditWidget;
    typedef std::shared_ptr<SubredditWidget> SubredditWidgetRef;

    class RedditContentProvider;
    typedef std::shared_ptr<RedditContentProvider> RedditContentProviderRef;

    class UserSearchContentProvider;
    typedef std::shared_ptr<UserSearchContentProvider> UserSearchContentProviderRef;

    class SubredditSearchContentProvider;
    typedef std::shared_ptr<SubredditSearchContentProvider> SubredditSearchContentProviderRef;

    class SubredditPostsContentProvider;
    typedef std::shared_ptr<SubredditPostsContentProvider> SubredditPostsContentProviderRef;

    class RedditContentListBoxView;
    typedef std::shared_ptr<RedditContentListBoxView> RedditContentListBoxViewRef;

    class UserMultiRedditsContentProvider;
    typedef std::shared_ptr<UserMultiRedditsContentProvider> UserMultiRedditsContentProviderRef;

    class UserMultiRedditsPage;
    typedef std::shared_ptr<UserMultiRedditsPage> UserMultiRedditsPageRef;

    class MultiRedditContentProvider;
    typedef std::shared_ptr<MultiRedditContentProvider> MultiRedditContentProviderRef;

    class MultiRedditFeedPage;
    typedef std::shared_ptr<MultiRedditFeedPage> MultiRedditFeedPageRef;

    class EditMultiPopup;
    typedef std::shared_ptr<EditMultiPopup> EditMultiPopupRef;

    class MessageWidget;
    typedef std::shared_ptr<MessageWidget> MessageWidgetRef;

    class MessagesContentProvider;
    typedef std::shared_ptr<MessagesContentProvider> MessagesContentProviderRef;

    class MessagesPage;
    typedef std::shared_ptr<MessagesPage> MessagesPageRef;

    class SendMessagePopup;
    typedef std::shared_ptr<SendMessagePopup> SendMessagePopupRef;
}
