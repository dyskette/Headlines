#pragma once

namespace util
{
    class Helpers
    {
    public:
        static void SplitString(const std::string& str, std::vector<std::string>& cont, char delim = ' ');
        static Json::Value GetJsonValueFromString(const std::string& str);
        static void OpenBrowser(const std::string& url);
        static std::string UnescapeHtmlForMarkdown(const std::string& data);
        static std::string UnescapeHtml(const std::string& data);
        static uint64_t GetSecondsSinceEpoch();
        static std::string TimeStampToDateString(uint64_t timeStamp);
        static std::string TimeStampToTimeAndDateString(uint64_t timeStamp);
        static void CopyToClipboard(const std::string& str);
        static xmlNodePtr FindXmlNodeByName(xmlNodePtr rootNode, const xmlChar* nodeName);
        static void HandleLink(std::string link);
        static void MSleep(int milliseconds);
        static unsigned int HexStringToUInt(std::string hex);
        static std::vector<std::string> SplitString(const std::string& str, const std::string& delim);
        static std::string FormatString(const std::string& fmt, ...);
        static std::string StringReplace(std::string str, const std::string& find, const std::string& replace);
        static void ApplyCardStyle(Gtk::Widget* widget, bool rounded);
        static std::string GetFileName(const std::string& filePath);
        static std::string GetFileExtension(const std::string& filePath);
        static std::string GetMimeType(const std::string& filePath);
        static std::unordered_map<std::string, std::string> GetUrlArgs(const std::string& url);
    };
}