#include <Application.h>
#include "NotificationManager.h"
#include "API/Message.h"
#include "API/RedditAPI.h"
#include "util/SimpleThread.h"
#include "UI/Pages/MessagesPage.h"

namespace util
{

    NotificationManager* NotificationManager::Get()
    {
        static NotificationManager s_Instance;
        return &s_Instance;
    }

    NotificationManager::NotificationManager()
    {
        Load();
        GetUnreadMessages();
        m_timeout_connection = Glib::signal_timeout().connect([this]()
        {
            GetUnreadMessages();
            return true;
        }, 300000);
    }

    NotificationManager::~NotificationManager()
    {
        m_timeout_connection.disconnect();
        if (m_UnreadNotificationsThread)
        {
            m_UnreadNotificationsThread->Cancel();
            m_UnreadNotificationsThread = nullptr;
        }
    }

    void NotificationManager::GetUnreadMessages()
    {
        if (m_UnreadNotificationsThread)
        {
            //it's been 5 minutes, just kill it.
            m_UnreadNotificationsThread->Cancel();
            m_UnreadNotificationsThread = nullptr;
        }

        m_UnreadNotificationsThread = new util::SimpleThread([this](util::ThreadWorker* worker)
        {
            if (!worker->IsCancelled())
            {
                for (const auto& message : m_UnreadMessages)
                {
                    Application::Get()->GetGtkApplication()->remove_action(util::Helpers::FormatString("view-notification-%s", message->GetID().c_str()));
                }

                m_UnreadMessages = api::RedditAPI::Get()->GetUnreadMessages();
            }
        },
        [this](bool cancelled, util::SimpleThread*)
        {
            if (!cancelled)
            {
                if (!m_UnreadMessages.empty())
                {
                    uint64_t lastNotificationTimestamp = 0;//m_SaveData["last_notification"].asUInt64();

                    for (const auto& message: m_UnreadMessages)
                    {
                        std::string actionName = util::Helpers::FormatString("view-notification-%s", message->GetID().c_str());
                        Application::Get()->GetGtkApplication()->add_action(actionName, [message]()
                        {
                            Application::Get()->OnAppActivate();
                            ui::MessagesPageRef messagesPage = std::make_shared<ui::MessagesPage>();
                            Application::Get()->AddPage(messagesPage);
                        });

                        if (message->GetCreated() > lastNotificationTimestamp)
                        {
                            auto notification = Gio::Notification::create("New message");
                            notification->set_default_action(util::Helpers::FormatString("app.%s", actionName.c_str()));
                            notification->set_body(util::Helpers::FormatString("%s: u/%s", message->GetSubject().c_str(), message->GetAuthor().c_str()));
                            Application::Get()->GetGtkApplication()->send_notification(util::Helpers::FormatString("headlines-new-message-%s", message->GetName().c_str()), notification);
                        }
                    }

                    m_SignalNewMessages.emit(m_UnreadMessages.size());
                    m_SaveData["last_notification"] = m_UnreadMessages[0]->GetCreated();
                    Save();
                }

                m_UnreadNotificationsThread = nullptr;
            }
        });
    }

    void NotificationManager::Save()
    {
        std::ofstream ofstream;
        ofstream.open(GetFilePath());

        Json::StreamWriterBuilder builder;
        Json::StreamWriter* writer = builder.newStreamWriter();
        writer->write(m_SaveData, &ofstream);
        ofstream.close();
    }

    void NotificationManager::Load()
    {
        bool dataLoaded = false;
        std::string filePath = GetFilePath();
        if (std::filesystem::exists(filePath))
        {
            std::ifstream ifstream;
            ifstream.open(GetFilePath());
            if (ifstream.is_open())
            {
                std::string lineValue;
                std::stringstream stringValue;
                while (!ifstream.eof())
                {
                    ifstream >> lineValue;
                    stringValue << lineValue;
                }

                m_SaveData = util::Helpers::GetJsonValueFromString(stringValue.str());
                dataLoaded = true;
            }
        }

        if (!dataLoaded)
        {
            m_SaveData = Json::Value();
            m_SaveData["last_notification"] = 0;
        }
    }

    std::string NotificationManager::GetFilePath()
    {
        std::stringstream ss;
        ss << g_get_user_config_dir() << "/headlines/notification_manager.json";
        return ss.str();
    }

    void NotificationManager::MarkMessagesRead()
    {
        for (const auto& message : m_UnreadMessages)
        {
            Application::Get()->GetGtkApplication()->remove_action(util::Helpers::FormatString("view-notification-%s", message->GetID().c_str()));
        }

        m_UnreadMessages.clear();
        m_SignalNewMessages.emit(m_UnreadMessages.size());
        api::RedditAPI::Get()->ReadAllMessages();
    }
}