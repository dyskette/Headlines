#include "SimpleThread.h"

#include <utility>
#include "ThreadWorker.h"

namespace util
{
    SimpleThread::SimpleThread(const std::function<void(ThreadWorker*)>& task, std::function<void(bool, util::SimpleThread*)> onFinished)
        : m_OnFinished(std::move(onFinished))
    {
        m_MainThreadDispatcher.connect([this]()
        {
            if (m_OnFinished != nullptr)
            {
                m_OnFinished(m_Worker->IsCancelled(), this);
            }

            delete m_Worker;
            m_Thread->join();
            delete m_Thread;
            delete this;
        });

        m_Worker = new ThreadWorker();
        m_Thread = new std::thread([this, task]()
        {
            m_Worker->Run([task](ThreadWorker* worker)
            {
                task(worker);
            }, [this](bool cancelled, ThreadWorker*)
            {
                if (!cancelled)
                {
                    m_MainThreadDispatcher.emit();
                }
            });
        });
    }

    void SimpleThread::Cancel()
    {
        m_Worker->Cancel();
    }
}
