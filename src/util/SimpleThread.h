#pragma once
#include "util/ThreadWorker.h"

namespace util
{
    class ThreadWorker;
    class SimpleThread
    {
    public:
        explicit SimpleThread(const std::function<void(ThreadWorker*)>& task, std::function<void(bool, SimpleThread*)> onFinished = nullptr);
        void Cancel();
    private:
        std::thread* m_Thread;
        ThreadWorker* m_Worker;
        std::function<void(bool, util::SimpleThread*)> m_OnFinished;
        Glib::Dispatcher m_MainThreadDispatcher;
    };
}
