#include "ThreadWorker.h"

namespace util
{
    ThreadWorker::ThreadWorker()
            : m_Mutex()
              , m_Cancelled(false)
              , m_Finished(false)
    {
    }

    void ThreadWorker::Cancel()
    {
        std::lock_guard<std::mutex> lock(m_Mutex);
        m_Cancelled = true;
    }

    bool ThreadWorker::IsCancelled() const
    {
        std::lock_guard<std::mutex> lock(m_Mutex);
        return m_Cancelled;
    }

    void ThreadWorker::Run(const std::function<void(ThreadWorker*)>& task, const std::function<void(bool, util::ThreadWorker*)>& onFinished)
    {
        {
            std::lock_guard<std::mutex> lock(m_Mutex);
            m_Finished = false;
            m_Cancelled = false;
        }

        task(this);

        {
            std::lock_guard<std::mutex> lock(m_Mutex);
            m_Finished = true;
            onFinished(m_Cancelled, this);
        }
    }
}